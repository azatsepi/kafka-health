import bia.kafka.health.consumer.*;
import bia.kafka.health.producer.*;
import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.test.context.junit4.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes =  {ProducerConfiguration.class, ConsumerConfiguration.class})
@Ignore
public class HealthCheckTest {

    @Autowired
    private Producer sender;

    @Autowired
    private Consumer receiver;

    @Test
    public void testReceiver() {

        sender.sendMessage("health-check-topic", "check?");

    }
}
