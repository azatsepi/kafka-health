package bia.kafka.health.producer;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.kafka.core.*;
import org.springframework.kafka.support.*;
import org.springframework.scheduling.annotation.*;
import org.springframework.util.concurrent.*;

public class Producer {
    private static final Logger LOGGER = LoggerFactory.getLogger(Producer.class);
    @Autowired
    private KafkaTemplate<Integer, String> kafkaTemplate;

    @Scheduled(fixedRate = 2000)
    public void doBySchedule() {
        sendMessage("health-check-topic", "check?");
    }

    public void sendMessage(String topic, String message) {
        ListenableFuture<SendResult<Integer, String>> future = kafkaTemplate.send(topic, message);

        future.addCallback(new ListenableFutureCallback<SendResult<Integer, String>>() {

            @Override
            public void onSuccess(SendResult<Integer, String> result) {
                LOGGER.info("sent message='{}' with offset={}", message, result.getRecordMetadata().offset());
            }

            @Override
            public void onFailure(Throwable ex) {
                LOGGER.error("unable to send message='{}'", message, ex);
            }
        });

    }
}
