package bia.kafka.health;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;

@SpringBootApplication
public class KafkaHealthMain {

    public static void main(String[] args) {
        SpringApplication.run(KafkaHealthMain.class, args);
    }
}
