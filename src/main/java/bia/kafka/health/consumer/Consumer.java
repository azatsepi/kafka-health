package bia.kafka.health.consumer;

import org.slf4j.*;
import org.springframework.kafka.annotation.*;

public class Consumer {
    private static final Logger LOGGER = LoggerFactory.getLogger(Consumer.class);

    @KafkaListener(topics = "health-check-topic")
    public void receiveMessage(String message) {
        LOGGER.info("received message= {} ", message);
    }
}
